require 'http'

class Lifx
  attr_accessor :label
  attr_reader :default_duration
  attr_reader :debug

  def initialize(label:, default_duration: 1.5, debug: false)
    @label = label
    @default_duration = default_duration
    @debug = debug
  end

  def off(duration: default_duration)
    result = client.put(state_endpoint, json: {
      power: 'off',
      duration: duration,
      fast: true
    })

    if debug
      puts result.inspect
      puts result.body
    end
  end

  def on(duration: default_duration, brightness: 1.0)
    result = client.put(state_endpoint, json: {
      power: 'on',
      duration: duration,
      fast: true,
      color: "white brightness:#{brightness}"
    })

    if debug
      puts result.inspect
      puts result.body
    end
  end

  private

  def client
    @client ||= HTTP.auth("Bearer #{ENV['LIFX_TOKEN']}")
  end

  def state_endpoint
    "https://api.lifx.com/v1/lights/label:#{label}/state"
  end
end

if $0 == __FILE__
  state = ARGV.shift || 'on'

  Lifx
    .new(label: 'Living Room', debug: true)
    .public_send(state.to_sym)
end
