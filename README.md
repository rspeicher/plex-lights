# plex-lights

Use Plex webhooks to control hue lights to set the mood.

## Deploy

```shell
cp .env.example .env
vim .env
bundle install

# See `bundle exec foreman help export` for more options
bundle exec foreman export upstart /etc/init -p 32405 -a plex-lights
```

## License

Copyright 2018 Robert Speicher. See LICENSE for details.
