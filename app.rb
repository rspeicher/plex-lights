# frozen_string_literal: true
require 'sinatra'
require 'json'

require_relative 'lifx'

class App < Sinatra::Application
  LIGHT   = ENV['LIGHT']
  ACCOUNT = ENV['ACCOUNT']
  PLAYER  = ENV['PLAYER']
  SECTION = ENV['SECTION']

  light = Lifx.new(label: LIGHT)

  get '/' do
    <<~MESSAGE
      Hello, Plex!

      Light:   #{LIGHT}
      Account: #{ACCOUNT}
      Player:  #{PLAYER}
      Section: #{SECTION}
    MESSAGE
  end

  post '/' do
    begin
      payload    = JSON.parse(params[:payload])
      account    = payload['Account']['title']
      player     = payload['Player']['title']
      section    = payload['Metadata']['librarySectionTitle']
      view_count = payload['Metadata']['viewCount'].to_i
    rescue StandardError
      return 400
    end

    return 400 unless account == ACCOUNT && player == PLAYER
    return 400 if SECTION && section != SECTION

    case payload['event']
    when 'media.play'
      # Don't turn off for movies we've already seen
      return 400 if view_count > 0

      light.off
    when 'media.resume'
      # Don't turn off for movies we've already seen
      return 400 if view_count > 0

      light.off(duration: 0.5)
    when 'media.pause'
      light.on(duration: 0.5, brightness: 0.4)
    when 'media.stop'
      light.on(brightness: 1.0)
    end

    200
  end
end
